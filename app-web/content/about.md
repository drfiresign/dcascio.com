+++
title = "About"
description = "this little site of mine"
page_template = "page.html"
+++

## Me
Welcome, I'm Dylan and I have a website.
You may have seen a website before.
If this is your first, then I hope this is the beginning of a beautiful friendship between you and the web.

## Technologies
### An incomplete list of the technologies this site uses

[Cloudflare]: https://www.cloudflare.com
[Pages]: https://developers.cloudflare.com/pages/
[Cloudflare Workers]: https://developers.cloudflare.com/workers/
[Zola]: https://www.getzola.org/
[anemone]: https://www.getzola.org/themes/anemone/
[archie-zola]: https://www.getzola.org/themes/archie-zola/

This site is built from scratch with [Zola] (with a lot of trial and error copying from the [anemone] and [archie-zola] themes).
It is hosted on [Cloudflare] using their static site feature, [Pages].
I'm currently experimenting with [Cloudflare Workers] to run backend serverless functions.
Those experiments aren't accessible at the moment.
