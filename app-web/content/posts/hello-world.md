+++
title = "Hello World"
date = 2024-02-21
description = "Introductory Post"
+++

Welcome to a another blog.

I'm in no rush to begin and I've never been much for blogging,
so who knows how often this will be updated.
I'm primarily interested in using this as a platform to experiment with different web technologies.
I might write up a post or two depending on what I learn.

-- dc
