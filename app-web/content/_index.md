+++
title = "home"
description = "this little site of mine"
+++
# a site for things

I'm a self-taught[^self-taught] software engineer working in site reliability.
Straightforward web design has never interested, me so this site is fairly basic style-wise.
It will primarily be a place for me to embarrass myself with my opinions and design acumen, as well as being a place where I can experiment with different technologies.

For more information on what kind of technologies I might be experimenting with please visit the [about](/about) page.

---

[^self-taught]: In the sense that I did not go to school for any of this. I have in fact been the beneficiary of a great deal of instruction from fellow engineers. Thank you to all the mentors I've met along the way.
