const title = document.querySelector('.site-title')
const asterisk = document.querySelector('#asterisk')
const description = document.querySelector('#site-description')

title.onmouseover = (e) => {
  asterisk.style.setProperty('rotate', 'y 0deg')
  description.style.setProperty('background-color', 'var(--fg-intense)')
}

title.onmouseout = (e) => {
  asterisk.style.setProperty('rotate', 'y -90deg')
  description.style.setProperty('background-color', '')
}
